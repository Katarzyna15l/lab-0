﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    public class Mężczyzna : Człowiek
    {
        private string zawod;

        public Mężczyzna()
            : base()
        {
            imie = "Jan";
            waga = 80;
            zawod = "Wykładowca";
        }

        public Mężczyzna(string imie, int waga, string zawod)
            : base(imie, waga)
        {
            this.imie = imie;
            this.waga = waga;
            this.zawod = zawod;
        }

        public override string przedstawSie()
        {
            string wiadomosc = base.przedstawSie() + ".\n";
            wiadomosc += "Jestem " + zawod + " i ważę " + waga + "kg.";

            return wiadomosc;
        }
    }
}
