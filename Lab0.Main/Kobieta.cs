﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    public class Kobieta : Człowiek
    {
        private string plec;

        public Kobieta()
            : base()
        {
            imie = "Ula";
            waga = 50;
            plec = "Kobieta";
        }

        public Kobieta(string imie, int waga)
            : base(imie, waga)
        {
            plec = "Kobieta";
        }

        public override string przedstawSie()
        {
            string wiadomosc = base.przedstawSie() + ".\n";
            wiadomosc += "Jestem " + plec + " i ważę " + waga + "kg.";

            return wiadomosc;
        }
    }
}
