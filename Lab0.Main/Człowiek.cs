﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace Lab0.Main
{
    public class Człowiek
    {
        protected string imie;
        protected int waga;

        public Człowiek()
        {
            imie = "Jan";
            waga = 80;
        }

        public Człowiek(string imie, int waga)
        {
            this.imie = imie;
            this.waga = waga;
        }

        public virtual string przedstawSie()
        {
            return "Nazywam się " + imie;
        }
    }
}
