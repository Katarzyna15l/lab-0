using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Program
    {
        static void Main(string[] args)
        {
            Człowiek czlowiek;

            bool wyjscie = false;
            while (!wyjscie)
            {
                Console.Clear();
                Console.WriteLine("1 - stwórz mężczyznę\n2 - stwórz kobietę\n3 - Nic nie rób i zakończ program");
                int opcja = Convert.ToInt32(Console.ReadLine());
                switch (opcja)
                {
                    case 1:
                        Console.WriteLine();
                        czlowiek = new Mężczyzna();
                        Console.WriteLine(czlowiek.przedstawSie());
                        Console.ReadKey();
                        break;
                    case 2:
                        Console.WriteLine();
                        czlowiek = new Kobieta();
                        Console.WriteLine(czlowiek.przedstawSie());
                        Console.ReadKey();
                        break;
                    case 3:
                        Console.WriteLine();
                        Console.WriteLine("Papapa");
                        wyjscie = true;
                        break;
                }
            }

            Console.ReadKey();
        }
    }
}