﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab0.Main;


namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(Człowiek);
        public static Type B = typeof(Kobieta);
        public static Type C = typeof(Mężczyzna);

        public static string commonMethodName = "przedstawSie";
    }
}
